public class FindPrimes {
    public static void main (String[] args) {
        int number = Integer.parseInt(args[0]);
        if (number <= 1){
            System.out.println("prime number is not found");
        }
        function(number);
    }
    public static void function(int number) {
        int a, b;
        for (a = 1; a < number; a++) {
            if (a == 1 || a == 0) {
                continue;
            }
            boolean c = true ;
            for (b= 2; b <= a / 2; ++b) {
                if (a % b == 0) {
                    c = false;
                    break;
                }
            }if(c)
                System.out.print(a + ",");
        }
    }
}
