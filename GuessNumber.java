
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int number =rand.nextInt(100); //generates a number between 0 and 99


		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		System.out.print("Can you guess it: ");

		boolean asd = true;

		int attempt = 1 ;


		while (asd){
			int guess = reader.nextInt(); //Read the user input

			if (guess!= -1) {
				if (number > guess) {
					System.out.println("Sorry!\nMine ise greater than your guess.");
					System.out.println("Type -1 to quit or guess another:");
					attempt += 1;


				}
				else if (number < guess) {
					System.out.println("Sorry!\nMine ise less than your guess.");
					System.out.println("Type -1 to quit or guess another:");
					attempt += 1;


				}
				else {
					System.out.println("Congratulations! You won after " + attempt + " attempts!");
					break;
				}



			}
			else {
				System.out.println("Sorry, the number was " + number);
				break;
			}
			}


	}


}